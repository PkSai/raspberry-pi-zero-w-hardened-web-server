#!/bin/sh

SERVER_IP="$1"
CLIENT1="$2"
CLIENT2="$3"

iptables --flush
iptables --policy INPUT DROP
iptables --policy OUTPUT ACCEPT 
iptables --policy FORWARD DROP

iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

iptables -A INPUT -i wlp3s0 -p tcp -s $CLIENT1 -d $SERVER_IP --dport 21 -j ACCEPT
iptables -A INPUT -i wlp3s0 -p tcp -s $CLIENT2 -d $SERVER_IP --dport 22 -j ACCEPT
iptables -A INPUT -i wlp3s0	-p icmp --icmp-type 8 -s $CLIENT2 -d $SERVER_IP -j ACCEPT 
iptables -A INPUT -i wlp3s0 -p icmp --icmp-type 8 -s $CLIENT2 -d $SERVER_IP -j DROP

