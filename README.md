# Raspberry Pi Zero W as a Web server

## Introduction
A Web Server can be conceptualized as a combination of hardware and software with the intention of fulfilling client HTTP/HTTPS requests on the public World Wide Web or on local private connections like LAN and WAN.

The following process will follow a headless setup using a Raspberry Pi Zero-W with the Raspberry OS lite image, then tunneling to it using SSH from another device and completing the setup as well as hardening the server using CLI only.

![image](https://gitlab.com/PkSai/raspberry-pi-zero-w-hardened-web-server/-/raw/master/screenshots/intro.jpg)


## Requirements
1. A Raspberry Pi Zero-W.
2. A Micro-SD card with at least 8GB of storage and a class 10 speed rating.
3. A power brick with a 2.5A power supply.
4. An optional case to protect the hardware components.

## Setting up 
1. **Download** the Raspberry OS Liteimage from the officaial [website](https://www.raspberrypi.org/software/operating-systems/).
	- Perform a shasum check:
		```
			$ sha256sum file.zip
		```
	- The output from the command can be compared with the sha256 integrity hash available on the official Raspberry website.
	- Unzip the zip file.
2. **Flashing the ISO**
	- After connecting the SD card using an adapter or any other method, we can check if the system recognizes the SD card by using the ***lsblk*** command. An easy way to identify the card is to check the storage capacity. After the SD card is recognized, we can simply flash the ISO image using a simple linux command line tool called ***dd***:
	```
		# dd if=/path/to/image/2021-01-11-raspios-buster-armhf-lite.img of=/dev/mmcblk0 bs=4M; status=progress
	```
	- if - stands for the input file, it will be the image file we unzipped earlier. 
	- of - is the output which will be pointed to our Micro-SD card. 
	- bs - is the block size.
	- status - is used to monitor the status of the flashing process.

 3. **Headless SSH**: Since this will be a headless setup, we will need to configure the Pi in a way that it would connect to our wireless network at first boot, also us to connect via SSH.
	- We will need to create a file called wpa_supplicant.conf using any text editor.
	```
		$ vim wpa_supplicant.conf
	```
	- The contents should contain the following:
	```
		ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
		update_config=1
		country=IN
		network={
			ssid="YourSSID"    
			psk="YourPassword"
			scan_ssid=1
		}
	```
	- The file will be delete after boot, you will need to create another one if you reboot it in the current state. 
	- Create a file called ssh without an extension, it may be empty or otherwise the contents won't matter.

4. **Booting up the Pi Zero-W**: After creating the files, we eject the micro SD card from our machine and insert it into the Rasberry Pi Zero-W. We connect the Pi Zero-W to a power source and wait for the it to boot up and connect to our wireless network.

5. **SSH into the pi**: This can be done in multiple ways, you can use the ssh command in any linux terminal or use the open-source application called putty.
	```
		$ ssh pi@<IP>
	```
	- IP is the ip address of your Pi Zero-W. In the case that the IP address is unknown, the command line utility ***nmap*** can be used:
	```
		# nmap -sn [device subnet]
	```
	- Device subnet can be determined using the ip addr command or its equivalent in a Linux Environment.
	```
		$ ip addr
	``` 
	- The nmap command is ran as root so as to display the device manufacturer and MAC address for easy identification.

6. **Setting up the Web Server**
	```
		# apt update
	```
	The Web Server software of choice is the nginx web server, known for its performance and reliability as well as the fact of it being free and open-source:
    ```
		# apt install nginx
	```
	We then need to start the server:
	```
		# /etc/init.d/nginx start

	```

> You will get an output similar to this
![image](https://gitlab.com/PkSai/raspberry-pi-zero-w-hardened-web-server/-/raw/master/screenshots/web1.png)

> You can view the default webpage on the Pi by going to https://localhost/, on any other machine > we need only to go to the address of the reaspberry pi as show below.


![image](https://gitlab.com/PkSai/raspberry-pi-zero-w-hardened-web-server/-/raw/master/screenshots/wb.png)

>The default file is located in /var/www/html
>The file being index.nginx-debian.html
>You can edit the file as you please and it will be served as such when requested.

## Additional configurations to ensure better security
1. **Changing the default ssh password, it is a terrible idea to leave the default passowrd as the same password for your web server.**
	```
		# passwd
	```
2. **Configure an the IDS (Intrusion Detection System)**
One could easily make a simple iptable chain rule, an example is attached. But using a smarter solution like an Intrusion Detection System or an Intrusion Prevention system seems to be a more feature rich approach. There are a few options to choose from, like SNORT, OSSEC. For now I will be using OSSEC. OSSEC is a host based intrusion detection system that performs log analysis, integrity checking, Windows registry monitoring, rootkit detection, real-time alerting and active response. It runs on most operating systems, including Linux, OpenBSD, FreeBSD, Mac OS X, Solaris and Windows.
	- Some pre-requisites: After running a system update, a few packages will be important later.
	```
		# apt install inotify-tools gcc zlib1g-dev

	```
	> In case you run into this compilation error: You can install the libevent-dev package as well.
	```
		ERROR: os_maild/sendmail.c:12:10: fatal error: event.h: No such file or directory #include <event.h>
	```
	- Dowload the tar file, the signature and importing the public key:
	```
		$ wget https://github.com/ossec/ossec-hids/archive/3.6.0.tar.gz

		$ wget </ossec-hids/releases/download/3.6.0/ossec-hids-3.6.0.tar.gz.asc

		$ gpg --import OSSEC-ARCHIVE-KEY.asc
		
	```		

	- Verify the signatures
	```
		$ gpg --verify version.asc version.tar.gz
	```
	> An output should be like this

![image](https://gitlab.com/PkSai/raspberry-pi-zero-w-hardened-web-server/-/raw/master/screenshots/gpg.png)

	- Extract the tar file
	```
		$ tar -xvzf version.tar.gz
	```
	- Some pcre2 regular expression are used and it may cause some errors, get the pcre2 archive and unpack it at src/external
	```
		$ wget https://ftp.pcre.org/pub/pcre/pcre2-10.32.tar.gz
	
		$ tar -xvf pcre2-10.{version}.tar.gz -C ossec-hids-{version}/src/external
	```
	
	- After extraction cd into the extracted directory and run the install script.
	```
		$ cd ossec-hids-version

		# sh install.sh
	```
> A look at the install script running
	```
	pi@raspberrypi:~$ cd ossec-hids-3.6.0/' 
	pi@raspberrypi:~/ossec-hids-3.6.0$ sudo sh install.sh 

	  ** Para instalação em português, escolha [br].
	  ** 要使用中文进行安装, 请选择 [cn].
   	  ** Fur eine deutsche Installation wohlen Sie [de].
	  ** Για εγκατάσταση στα Ελληνικά, επιλέξτε [el].
	  ** For installation in English, choose [en].
	  ** Para instalar en Español , eliga [es].
	  ** Pour une installation en français, choisissez [fr]
	  ** A Magyar nyelvű telepítéshez válassza [hu].
	  ** Per l'installazione in Italiano, scegli [it].
	  ** 日本語でインストールします．選択して下さい．[jp].
	  ** Voor installatie in het Nederlands, kies [nl].
	  ** Aby instalować w języku Polskim, wybierz [pl].
	  ** Для инструкций по установке на русском ,введите [ru].
	  ** Za instalaciju na srpskom, izaberi [sr].
	  ** Türkçe kurulum için seçin [tr].
	  (en/br/cn/de/el/es/fr/hu/it/jp/nl/pl/ru/sr/tr) [en]: en
	
	 OSSEC HIDS v3.6.0 Installation Script - http://www.ossec.net
	 
	 You are about to start the installation process of the OSSEC HIDS.
	 You must have a C compiler pre-installed in your system.
	 
	  - System: Linux raspberrypi 5.10.11+
	  - User: root
	  - Host: raspberrypi
	
	
	  -- Press ENTER to continue or Ctrl-C to abort. --
	
	
	1- What kind of installation do you want (server, agent, local, hybrid or help)? server
	
	  - Server installation chosen.
	
	2- Setting up the installation environment.
	
	 - Choose where to install the OSSEC HIDS [/var/ossec]:  
	
	    - Installation will be made at  /var/ossec .
	
	3- Configuring the OSSEC HIDS.
	
	  3.1- Do you want e-mail notification? (y/n) [y]: n
	
	   --- Email notification disabled.
	
	  3.2- Do you want to run the integrity check daemon? (y/n) [y]: y
	
	   - Running syscheck (integrity check daemon).
	
	  3.3- Do you want to run the rootkit detection engine? (y/n) [y]: y
	
	   - Running rootcheck (rootkit detection).
	
	  3.4- Active response allows you to execute a specific 
	       command based on the events received. For example,
	       you can block an IP address or disable access for
	       a specific user.  
	       More information at:
	       http://www.ossec.net/en/manual.html#active-response
	       
	   - Do you want to enable active response? (y/n) [y]: y
	
	     - Active response enabled.
	   
	   - By default, we can enable the host-deny and the 
	     firewall-drop responses. The first one will add
	     a host to the /etc/hosts.deny and the second one
	     will block the host on iptables (if linux) or on
	     ipfilter (if Solaris, FreeBSD or NetBSD).
	   - They can be used to stop SSHD brute force scans, 
	     portscans and some other forms of attacks. You can 
	     also add them to block on snort events, for example.
	
	   - Do you want to enable the firewall-drop response? (y/n) [y]: y
	
	     - firewall-drop enabled (local) for levels >= 6
	
	   - 
	      - 192.168.43.xxx [Local Ip address]
	
	   - Do you want to add more IPs to the white list? (y/n)? [n]: n
	
	  3.5- Do you want to enable remote syslog (port 514 udp)? (y/n) [y]: y
	
	   - Remote syslog enabled.
	
	  3.6- Setting the configuration to analyze the following logs:
	    -- /var/log/messages
	    -- /var/log/auth.log
	    -- /var/log/syslog
	    -- /var/log/dpkg.log
	    -- /var/log/nginx/access.log (apache log)
	    -- /var/log/nginx/error.log (apache log)
	
	 - If you want to monitor any other file, just change 
	   the ossec.conf and add a new localfile entry.
	   Any questions about the configuration can be answered
	   by visiting us online at http://www.ossec.net .
	   
	   
	   --- Press ENTER to continue ---
	```
	
- To start the service, use:
	
	```
		# /var/ossec/bin/ossec-control start
		Starting OSSEC HIDS v3.6.0 (by Trend Micro Inc.)...
		Started ossec-maild...
		Started ossec-execd...
		Started ossec-analysisd...
		Started ossec-logcollector...
		Started ossec-syscheckd...
		Started ossec-monitord...
		Completed.
	```
- It can be stopped using a similar command	
	```	
		# /var/ossec/bin/ossec-control stop
	```
	
- You can further configure OSSEC HIDS by editing the configuration file:
	
	```
		# vim /var/ossec/etc/ossec.conf
	```
- The rules are located under the directory/var/ossec/rules/. The rules for local system files are set on the file /var/ossec/rules/local_rules.xml

3. **Changing the default ports for FTP,SSH:** A full network scan would be needed to determine the new ports, hence the rationale for changing the default ports would be that if an attacker performs such a scan it would be easily detected.
	- You can change the default ftp port by simply editing the file in the format "PORT XXXX" Where X's represent any port of your choosing
	```
	$ vim /etc/proftpd/proftpd.conf
	```
	- Likewise for ssh, we simply edit the ssh configuration file
	```
	$ vim /etc/ssh/sshd_config
	```
- The same can be done for SMTP,SFTP in their respective configuration files.

## Conclusion
A different intrusion detection system like SNORT or an intrusion prevention ssytem like fail2ban would in my opinion be better choices, SNORT in my usage seemed to be more flexible when it comes to configuration.

A perfectly configured system does not exist but it is an ideal to strive for, this system is by far not that ideal but any step in that direction is a good one. Below is an image of lynx running the newly configure web server.
![image](https://gitlab.com/PkSai/raspberry-pi-zero-w-hardened-web-server/-/raw/master/screenshots/wlynx.png)
